package com.firststeps.degtyar.weatherlocation;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.test.AndroidTestCase;


/**
 * Created by Ksyu on 09.02.2016.
 */
public class ResourceTest extends AndroidTestCase {

    Context testContext;
    Resources testRes;
    int primary_color;
    Drawable icon;
    String inet_connection = "No internet connection";
    String app_id = "51219858246d90862e6b04b1ece5a3567";

    public void setUp() throws Exception {
        super.setUp();
        testContext = mContext;
        testRes = testContext.getResources();

    }

    public void tearDown() throws Exception {

    }

    public void testFoo() throws Exception {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            icon = testRes.getDrawable(R.drawable.update, testContext.getTheme());
        } else {
            icon = testRes.getDrawable(R.drawable.update);
        }

        assertNotNull(icon);
    }

    public void testPrimaryColor() throws Exception {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            primary_color = testRes.getColor(R.color.color_primary, testContext.getTheme());
        } else {
            primary_color = testRes.getColor(R.color.color_primary);
        }
        assertNotNull(primary_color);
    }

    public void testInetConnectionString() throws Exception {
        assertEquals(inet_connection, testRes.getString(R.string.inet_connection));
    }

    public void testAppId() throws Exception {
        assertEquals(app_id, testRes.getString(R.string.app_id));
    }

    public void testWeatherFragment() throws Exception {
        assertNotNull(testRes.getLayout(R.layout.weather_fragment));
    }

    public void testDimen() throws Exception {
        assertEquals("17dp", testRes.getDimension(R.dimen.weather_pad_10));
    }

    public void testInetConnectionAvail() throws Exception {

        ConnectivityManager cm =
                (ConnectivityManager) testContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        assertNotNull(netInfo);
        assertTrue(netInfo.isConnectedOrConnecting());

    }

}
