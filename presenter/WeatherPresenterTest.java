package com.firststeps.degtyar.weatherlocation.presenter;

import android.app.Activity;
import android.test.AndroidTestCase;

import com.firststeps.degtyar.weatherlocation.model.Rain;
import com.firststeps.degtyar.weatherlocation.model.WeatherModel;
import com.firststeps.degtyar.weatherlocation.model.Wind;
import com.firststeps.degtyar.weatherlocation.view.MainActivity;
import com.google.gson.Gson;

import junit.framework.TestCase;

import java.util.ArrayList;

/**
 * Created by Ksyu on 10.02.2016.
 */
public class WeatherPresenterTest extends AndroidTestCase {

    WeatherPresenter weatherPresenter;
    WeatherModel modelFromFunction;
    WeatherModel modelFromGson;
    String responseString =

            "{\"coord\":{\"lon\":37.8,\"lat\":48},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"base\":\"cmc stations\"," +
                    "\"main\":{\"temp\":2.21,\"pressure\":1011.34,\"humidity\":81,\"temp_min\":2.21,\"temp_max\":2.21,\"sea_level\":1035.84,\"grnd_level\":1011.34}," +
                    "\"wind\":{\"speed\":4.92,\"deg\":129.5},\"clouds\":{\"all\":0},\"dt\":1455110063,\"sys\":{\"message\":0.003,\"country\":\"UA\"," +
                    " \"sunrise\":1455079424,\"sunset\":1455115379},\"id\":709717,\"name\":\"Donetsk\",\"cod\":200}";

    public void setUp() throws Exception {
        super.setUp();
        weatherPresenter = new WeatherPresenter(mContext);

        //Модель распарсенная функцией
        modelFromFunction = weatherPresenter.parseWeatherInfo(responseString);

        //Модель распарсенная Gson
        Gson gson = new Gson();
        modelFromGson = gson.fromJson(responseString, WeatherModel.class);
    }

    public void tearDown() throws Exception {

    }

    public void testParseWeatherInfo() throws Exception {
        assertNotNull(modelFromFunction);
        assertNotNull(modelFromGson);
    }

    public void testParseModelEquals() throws Exception {
        assertEquals(modelFromFunction, modelFromGson); // false
    }

    public void testParseModelCoord() {
        assertEquals(modelFromFunction.getCoord(), modelFromGson.getCoord()); //false
    }

    public void testModelMinMaxTemp() throws Exception {
        assertEquals(modelFromFunction.getMain().getTemp_max(), modelFromGson.getMain().getTemp_max());//true
    }

    public void testModelWind() throws Exception {

        WeatherModel manualModel;
        manualModel = new WeatherModel();

        Wind wind = new Wind();
        wind.setDeg("130.5");
        wind.setSpeed("4.92");
        manualModel.setWind(wind);

        assertEquals(manualModel.getWind().getDeg(), modelFromFunction.getWind().getDeg());     //false
        assertEquals(manualModel.getWind().getSpeed(), modelFromFunction.getWind().getSpeed()); //true
    }

    public void testGetElement() throws Exception {

        assertNotNull((new WeatherModel()).getCod());
        assertNull((new ArrayList()).get(0));           //тестирование IndexOutOfBoundsException
    }
}
