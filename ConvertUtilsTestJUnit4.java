package com.firststeps.degtyar.weatherlocation;

import android.support.test.runner.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


import static org.junit.Assert.*;

/**
 * Created by Ksyu on 10.02.2016.
 */

public class ConvertUtilsTestJUnit4 {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testConvertUnixTimeToLocal() throws Exception {

        assertEquals("20:46", ConvertUtils.convertUnixTimeToLocal("1435610796"));
        assertEquals("07:54", ConvertUtils.convertUnixTimeToLocal("1435650870"));

    }

    @Test
    public void testGetDateTime() throws Exception {

        assertEquals("10.02.16", ConvertUtils.getDateTime(true));
        assertEquals("10.02.16 00:12", ConvertUtils.getDateTime(false));
    }
}