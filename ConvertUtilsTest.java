package com.firststeps.degtyar.weatherlocation;

import junit.framework.TestCase;

/**
 * Created by Ksyu on 09.02.2016.
 */
public class ConvertUtilsTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {

    }

    public void testConvertUnixTimeToLocal() throws Exception {

        assertEquals("20:46", ConvertUtils.convertUnixTimeToLocal("1435610796"));
        assertEquals("07:54", ConvertUtils.convertUnixTimeToLocal("1435650870"));
    }

    public void testGetDateTime() throws Exception {

        assertEquals("10.02.16", ConvertUtils.getDateTime(true));
        assertEquals("10.02.16 00:12", ConvertUtils.getDateTime(false));
    }
}