package com.firststeps.degtyar.weatherlocation;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.test.AndroidTestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Ksyu on 12.02.2016.
 */
public class ResourceTestJUnit4 extends AndroidTestCase {

    Context testContext;
    Resources testRes;
    int primary_color;
    Drawable icon;
    String inet_connection = "No internet connection";
    String app_id = "51219858246d90862e6b04b1ece5a3567";

    @Before
    public void setUp() throws Exception {
        testContext = mContext;
        testRes = testContext.getResources();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testTestFoo() throws Exception {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            icon = testRes.getDrawable(R.drawable.update, testContext.getTheme());
        } else {
            icon = testRes.getDrawable(R.drawable.update);
        }

        assertNotNull(icon);
    }

    @Test
    public void testTestPrimaryColor() throws Exception {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            primary_color = testRes.getColor(R.color.color_primary, testContext.getTheme());
        } else {
            primary_color = testRes.getColor(R.color.color_primary);
        }
        assertNotNull(primary_color);
    }

    @Test
    public void testTestInetConnectionString() throws Exception {
        assertEquals(inet_connection, testRes.getString(R.string.inet_connection));
    }

    @Test
    public void testTestAppId() throws Exception {
        assertEquals(app_id, testRes.getString(R.string.app_id));
    }

    @Test
    public void testTestWeatherFragment() throws Exception {
        assertNotNull(testRes.getLayout(R.layout.weather_fragment));

    }

    @Test
    public void testTestDimen() throws Exception {
        assertEquals("17dp", testRes.getDimension(R.dimen.weather_pad_10));
    }

    @Test
    public void testTestInetConnectionAvail() throws Exception {
        ConnectivityManager cm =
                (ConnectivityManager) testContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        assertNotNull(netInfo);
        assertTrue(netInfo.isConnectedOrConnecting());
    }
}